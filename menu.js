jQuery( document ).ready(function() {

    //add aria-label to identify this as the main menu
    jQuery('#navbar-main').attr('aria-label', 'main');

    // create visibility styles
    mm_show_styles = {'height': 'auto', 'max-height': 'none', 'display': 'block','visibility': 'visible', 'opacity': '1' }; // show state
    mm_hide_styles = {'height': '0px',  'max-height': '0px',  'display': 'none', 'visibility': 'hidden', 'opacity': '0', }; // hide state
    // set initial visibility states
    jQuery('#navbar-main section.ultimenu__flyout').css(mm_hide_styles); // set initial visibility of submenu items

    // add toggler element
    jQuery('#navbar-main section.ultimenu__flyout').parent().find('> a').after('<button aria-haspopup="true" aria-expanded="false" class="wcag21-toggle"><span aria-hidden="true">&#9660;</span><span class="sr-only">Toggle sub menu</span></button>'); // add toggler element to 'li's with submenu

    // Keyboard toggle
    jQuery('.wcag21-toggle').bind('click', function (){
        var expandedState = jQuery(this).attr('aria-expanded');

        if (expandedState == 'true') {
            jQuery(this).attr('aria-expanded', 'false');
            jQuery(this).next().css(mm_hide_styles);
        }
        else {
            jQuery(this).attr('aria-expanded', 'true');
            jQuery(this).next().css(mm_show_styles);
        }
    });
    // mouse hover
    jQuery('#navbar-main section.ultimenu__flyout').parent().bind('mouseenter', function() {
        jQuery(this).find('> button').attr('aria-expanded', 'true');
        jQuery(this).find('section.ultimenu__flyout').css(mm_show_styles);
    });
    jQuery('#navbar-main section.ultimenu__flyout').parent().bind('mouseleave', function() {
        jQuery(this).find('> button').attr('aria-expanded', 'false');
        jQuery(this).find('section.ultimenu__flyout').css(mm_hide_styles);
    });
});
